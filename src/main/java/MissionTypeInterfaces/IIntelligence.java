package MissionTypeInterfaces;

public interface IIntelligence {
    public String getSensorExtensions();
}
