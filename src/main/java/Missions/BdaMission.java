package Missions;

import AerialVehicles.*;
import MissionTypeInterfaces.IBda;

public class BdaMission extends Mission {

    private String objective;

    public BdaMission() {
        super();
        System.out.println("select an aircraft:\n" +
                "1. F16.\n" +
                "2. Kochav.\n" +
                "3. Zik.\n" +
                "4. Shoval.");

        switch (in.nextInt()) {
            case 1:
                this.setAircraft(new F16());
                break;
            case 2:
                this.setAircraft(new Kochav());
                break;
            case 3:
                this.setAircraft(new Zik());
                break;
            case 4:
                this.setAircraft(new Shoval());
                break;
        }
        in.nextLine();
        System.out.println("enter an objective: ");
        String o = in.nextLine();
        setObjective(o);
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    @Override
    public String executeMission() {
        return (getPilotName() + ": " + getAircraft().getAircraftName() + " taking picture of suspect " + objective + " with: " + ((IBda)getAircraft()).getCameraExtensions() + " camera") ;
    }
}
