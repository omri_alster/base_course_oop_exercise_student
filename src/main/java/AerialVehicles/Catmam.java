package AerialVehicles;

import Entities.Coordinates;
import MissionTypeInterfaces.IIntelligence;

import java.util.Scanner;

public abstract class Catmam extends AerialVehicle implements IIntelligence {

    public static Scanner in = new Scanner(System.in);

    private String sensor;

    public Catmam() {
        System.out.println("choose Sensor Type:\n" +
                "1. InfraRed.\n" +
                "2. Elint.");
        switch (in.nextInt()) {
            case 1:
                this.sensor = "InfraRed";
                break;
            case 2:
                this.sensor = "Elint";
                break;
        }
    }

    public String hoverOverLocation(Coordinates destination) {
        System.out.println("Hovering over: " + destination);
        setFlightStatus(FLYING);
        return ("Hovering over: " + destination);
    }

    @Override
    public String getSensorExtensions() {
        return this.sensor;
    }
}
