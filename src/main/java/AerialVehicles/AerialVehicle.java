package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {

    final static Coordinates DEFAULT_MOTHER_BASE = new Coordinates(32.123123, 41.123123);

    public static int NOT_READY_TO_FLY = 0;
    public static int READY_TO_FLY = 1;
    public static int FLYING = 2;
    public static double MAX_HOURS_WITHOUT_FIX;

    private int hoursOfFlightSinceLastFix;
    private int flightStatus;
    private Coordinates motherBase;

    public AerialVehicle() {
        this.hoursOfFlightSinceLastFix = 0;
        this.flightStatus = READY_TO_FLY;
        this.motherBase =  DEFAULT_MOTHER_BASE;
    }

    public void flyTo(Coordinates destination) {
        if (flightStatus == READY_TO_FLY) {
            setFlightStatus(FLYING);
            System.out.println("flight to: " + destination);
        } else
            System.out.println("Aerial Vehicle isn't ready to fly");
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: " + destination);
        check();
    }

    public void check() {
        if (hoursOfFlightSinceLastFix >= MAX_HOURS_WITHOUT_FIX) {
            setFlightStatus(NOT_READY_TO_FLY);
            repair();
        } else
            setFlightStatus(READY_TO_FLY);
    }

    private void repair() {
        resetHoursOfFlightSinceLastFix();
        setFlightStatus(READY_TO_FLY);
    }

    public void resetHoursOfFlightSinceLastFix() {
        this.hoursOfFlightSinceLastFix = 0;
    }

    public void setFlightStatus(int status) {
        this.flightStatus = status;
    }

    public Coordinates getMotherBase() { return motherBase; }

    public void setMotherBase(Coordinates motherBase) { this.motherBase = motherBase; }

    public abstract String getAircraftName();
}
