package AerialVehicles;

import MissionTypeInterfaces.IBda;

public abstract class Hermes extends Catmam implements IBda {

    private String camera;

    public Hermes() {
        super();
        MAX_HOURS_WITHOUT_FIX = 100;
        System.out.println("enter camera type:\n" +
                "1. Regular.\n" +
                "2. Thermal.\n" +
                "3. NightVision.");

        switch (in.nextInt()) {
            case 1:
                this.camera = "Regular";
                break;
            case 2:
                this.camera = "Thermal";
                break;
            case 3:
                this.camera = "NightVision";
                break;
        }
    }

    @Override
    public String getCameraExtensions() {
        return this.camera;
    }
}
