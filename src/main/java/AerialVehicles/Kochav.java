package AerialVehicles;

import MissionTypeInterfaces.IAttack;
import Missions.AttackMission;

public class Kochav extends Hermes implements IAttack {

    private int amountOfMissile;
    private String kindOfMissile;

    public Kochav() {
        super();
        System.out.println("enter missile type:\n" +
                "1. Python.\n" +
                "2. Amram\n" +
                "3. Spice250.");

        switch (in.nextInt()) {
            case 1:
                this.kindOfMissile = "Python";
                break;
            case 2:
                this.kindOfMissile = "Amram";
                break;
            case 3:
                this.kindOfMissile = "Spice250";
                break;
        }

    }

    @Override
    public String getAircraftName() {
        return "Kochav";
    }

    @Override
    public String getMissileExtensions() {
        return this.kindOfMissile + "X" + this.amountOfMissile;
    }
}
