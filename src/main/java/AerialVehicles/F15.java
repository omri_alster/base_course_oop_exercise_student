package AerialVehicles;


import MissionTypeInterfaces.IIntelligence;
import Missions.IntelligenceMission;

import java.util.Scanner;

public class F15 extends AttackAircraft implements IIntelligence {

    public static Scanner in = new Scanner(System.in);

    private String sensor;

    public F15() {
        super();
        System.out.println("choose Sensor Type:\n" +
                "1. InfraRed.\n" +
                "2. Elint.");
        switch (in.nextInt()) {
            case 1:
                this.sensor = "InfraRed";
                break;
            case 2:
                this.sensor = "Elint";
                break;
        }
    }

    @Override
    public String getAircraftName() {
        return "F15";
    }

    @Override
    public String getSensorExtensions() {
        return this.sensor;
    }
}
