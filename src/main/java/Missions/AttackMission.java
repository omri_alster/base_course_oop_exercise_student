package Missions;
import AerialVehicles.*;
import MissionTypeInterfaces.IAttack;

import java.util.Scanner;

public class AttackMission extends Mission {

    public static Scanner in = new Scanner(System.in);

    private String target;

    public AttackMission() {
        super();
        System.out.println("select an aircraft:\n" +
                "1. F15.\n" +
                "2. F16.\n" +
                "3. Kochav.\n" +
                "4. Eithan.\n" +
                "5. Shoval.");

        switch (in.nextInt()) {
            case 1:
                this.setAircraft(new F15());
                break;
            case 2:
                this.setAircraft(new F16());
                break;
            case 3:
                this.setAircraft(new Kochav());
                break;
            case 4:
                this.setAircraft(new Eitan());
                break;
            case 5:
                this.setAircraft(new Shoval());
                break;
        }
        in.nextLine();
        System.out.println("enter a target to attack: ");
        this.setTarget(in.nextLine());
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String executeMission() {
        return getPilotName() + ": " + getAircraft().getAircraftName() + " Attacking suspect " + target + " with: " + ((IAttack) getAircraft()).getMissileExtensions();
    }
}
