package Missions;

import AerialVehicles.*;
import Entities.Coordinates;

import java.util.Scanner;

public abstract class Mission {

    public static Scanner in = new Scanner(System.in);

    private String pilotName;
    private Coordinates destination;
    private AerialVehicle aircraft;

    public Mission() {
        System.out.println("enter pilot name:");
        this.pilotName = in.nextLine();
        System.out.println("for the destination - enter two numbers, latitude first, then longtitude:");
        newDestination(in.nextDouble(), in.nextDouble());
        System.out.println("for the aircraft mother base - enter two numbers, latitude first, then longtitude:");
    }

    public void begin() {
        this.aircraft.flyTo(this.destination);
        System.out.println("Beginning Mission!");
    }

    public void cancel() {
        this.aircraft.land(this.aircraft.getMotherBase());
        System.out.println("Abort Mission!");
    }

    public void finish() {
        System.out.println(executeMission());
        getAircraft().land(aircraft.getMotherBase());
        System.out.println("Finish Mission!");
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public Coordinates getDestination() {
        return this.destination;
    }

    public void setDestination(Double latitude, Double longtitude) {
        this.destination.setLatitude(latitude);
        this.destination.setLongitude(longtitude);
    }

    public void newDestination(Double latitude, Double longtitude) {
        this.destination = new Coordinates(latitude, longtitude);
    }

    public AerialVehicle getAircraft() {
        return aircraft;
    }

    public void setAircraft(AerialVehicle aircraft) {
        this.aircraft = aircraft;
    }

    public abstract String executeMission();
}
